module.exports = async (func, args) => {
    try{
        await func(...args);
    } catch (e) {
        if(e.message.search('revert') >= 0) return true;
    }
}