pragma solidity ^0.4.24;
import "./ownable.sol";
import "./safemath.sol";

/**
 * @title Standard ERC20 token
 */

contract ERC20 is Ownable {
    using SafeMath for uint256;

    mapping (address => uint256) _balances;
    mapping (address => mapping(address => uint256)) _allowed;

    uint256 private _totalSupply;
    uint256 public decimals;
    string public name;
    string public symbol;

    constructor() public {
        name = "Dukascoin";
        symbol = "DUKA";
        decimals = 9;
        _totalSupply = 20000000000000000000;
        _balances[msg.sender] = _totalSupply;
    }

    /*
     * @dev Returns the total amount of tokens
     */

    function totalSupply() public view returns (uint256) {
        return _totalSupply;
    }

    /**
    * @dev Gets the balance of the specified address.
    * @param tokenOwner The address to query the balance of.
    * @return A uint256 representing the amount owned by the passed address.
    */

    function balanceOf(address tokenOwner) public view returns (uint256 balance) {
        return _balances[tokenOwner];
    }

    /**
    * @dev Function to check the amount of tokens that an owner allowed to a spender.
    * @param tokenOwner address The address which owns the funds.
    * @param _spender address The address which will spend the funds.
    * @return A uint256 specifying the amount of tokens still available for the spender.
    */

    function allowance(address tokenOwner, address _spender) public view returns (uint256 remaining){
        return _allowed[tokenOwner][_spender];
    }

    /**
    * @dev Transfer tokens to a specified address.
    * @param to The address to transfer to.
    * @param tokens The amount to be transferred.
    * @return True if the operation was successful.
    */

    function transfer(address to, uint256 tokens) public returns (bool success){
        if(_balances[msg.sender] > tokens){
            _balances[msg.sender] = _balances[msg.sender].sub(tokens);
            _balances[to] = _balances[to].add(tokens);
            emit Transfer(msg.sender, to, tokens);
            return true;
        }

    }

    /**
    * @dev Approve the passed address to spend the specified amount of tokens on behalf of msg.sender.
    * @param _spender The address which will spend the funds.
    * @param tokens The amount of tokens to be spent.
    * @return Returns true if the operation was successful.
    */

    function approve(address _spender, uint256 tokens) public returns (bool success){
        if(_balances[msg.sender] > tokens){
            _allowed[msg.sender][_spender] = tokens;
            emit Approval(msg.sender, _spender, tokens);
            return true;
        }
    }

    /**
    * @dev Increases the amount a spender is allowed to spend.
    * @param _spender Address to a spender's account.
    * @param _addedValue Amount to be added to the allowance.
    * @return Returns true if the operation was successful.
    */

    function increaseApproval(address _spender, uint256 _addedValue) public returns (bool){
        if(_allowed[msg.sender][_spender] >= 0){
            _allowed[msg.sender][_spender] = _allowed[msg.sender][_spender].add(_addedValue);
            emit Approval(msg.sender, _spender, _addedValue);
            return true;
        }
    }

    /**
    * @dev Decreases the amount a spender is allowed to spend.
    * @param _spender Address to a spender's account.
    * @param _addedValue Amount to be removed from the allowance.
    * @return Returns true if the operation was successful.
    */

    function decreaseApproval(address _spender, uint256 _addedValue) public returns (bool){
        if(_allowed[msg.sender][_spender] >= 0){
            _allowed[msg.sender][_spender] = _allowed[msg.sender][_spender].sub(_addedValue);
            emit Approval(msg.sender, _spender, _addedValue);
            return true;
        }
    }
    
    /**
    * @dev Transfer tokens from one address to another.
    * @param _from address The address which you want to send tokens from.
    * @param _to address The address which you want to transfer to.
    * @param _value uint256 the amount of tokens to be transferred.
    * @return Returns true if the operation was successful.
    */

    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success){
        if(_balances[_from] >= _value){
            _balances[_from] = _balances[_from].sub(_value);
            _balances[_to] = _balances[_to].add(_value);
            emit Transfer(_from, _to, _value);
            return true;
        }
    }

    /**
    * @dev Removes a certain amount of tokens from the total supply.
    * @param from Address to burn tokens from.
    * @param tokens Amount to be burned.
    */

    function burn(address from, uint256 tokens) public onlyOwner {
        if(_balances[from] >= tokens){
            _balances[from] = _balances[from].sub(tokens);
            _totalSupply = _totalSupply.sub(tokens);
            emit Burn(from, tokens);
        }
    }

    /**
    * @dev Adds a certain  amount to the total supply sending them to a certain address.
    * @param to Address the tokens are to be added to.
    * @param tokens Amount to be added to the address and total supply.
    */

    function mint(address to, uint256 tokens) public onlyOwner {
        _balances[to] = _balances[to].add(tokens);
        _totalSupply = _totalSupply.add(tokens);
        emit Mint(to, tokens);
    }

    function transferOwnership(address newOwner) public onlyOwner {
        _owner = newOwner;
    }

    event Transfer(address indexed from, address indexed to, uint256 tokens);

    event Approval(address indexed tokenOwner, address indexed spender, uint256 tokens);

    event Mint(address indexed addedTo, uint256 tokens);

    event Burn(address indexed burnedFrom, uint256 tokens);

}