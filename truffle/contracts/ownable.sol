pragma solidity ^0.4.24;

contract Ownable {
    address public _owner;

    constructor() internal{
        _owner = msg.sender;
    }

    /**
    * @dev Returns the owner.
    * @return The owner's address. 
    */

    function _owner() public  view returns (address){
        return _owner;
    }

    /**
    * @dev Checks if the user passed to the function is the owner.
    * @return Returns true if the user is the owner 
    */

    function isOwner() public view returns(bool){
        return msg.sender == _owner;
    }

    /**
    * @dev Throws if called by any other account than the owner
    */
    
    modifier onlyOwner(){
        require(isOwner(), "Not the owner");
        _;
    }
}