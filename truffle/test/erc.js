
const ERC20 = artifacts.require('./ERC20');

const revert = require('./helpers/revert');

contract('ERC20', accounts => {
    let contract, sender, user1, user2;
    const defaultSupply = 2e19;

    const metaData = {
        name: 'Dukascoin',
        symbol: 'DUKA',
        decimals: 9
    }


    before('setup', async () => {
        contract = await ERC20.new();
        [sender, user1, user2] = accounts;
    });

    describe('Init', async () => {
        it('Should initialize the contract with 2e19 tokens', async () => {
            const supply = await contract.totalSupply();
            assert.equal(supply, defaultSupply, 'The contract was not initialized with the amount of tokens specified');
        });
        it('Meta data should be correct', async () => {
            const currentMeta = {
                name: await contract.name.call(),
                symbol: await contract.symbol.call(),
                decimals: (await contract.decimals.call()).toNumber()
            }

            assert.deepEqual(metaData, currentMeta, 'Meta data is not correct');

        })
    });

    describe('View Operations', async () => {
        it('Should return the balance of a user', async () => {
            const balance = await contract.balanceOf(sender);
            assert.equal(balance, defaultSupply, 'The correct balance was not returned');
        });
        it('Should return the allowance of a user', async () => {
            const allowance = await contract.allowance(sender, user1);
            assert.equal(allowance, 0, 'The correct allowance value was not returned');
        });
    });

    describe('Allowance Operations', () => {
        it('Should set an allowance value for a user', async () => {
            await contract.approve(user1, 300);
            const allowance = await contract.allowance(sender, user1);
            assert.equal(allowance, 300, 'An allowance of 300 tokens was not set');
        });
        it('Should increase the allowance of a user', async () => {
            await contract.increaseApproval(user1, 30);
            const allowance = await contract.allowance(sender, user1);
            assert.equal(allowance, 330, 'The allowance value was not increased by a specified amount');
        });
        it('Should decrease the allowance of a user', async () => {
            await contract.decreaseApproval(user1, 30);
            const allowance = await contract.allowance(sender, user1);
            assert.equal(allowance, 300, 'The allowance value was not increased by a specified amount');
        });
    });

    describe('Transfer Operations', () => {
        it('Should send tokens to a user', async () => {
            await contract.transfer(user2, 10);
            const balance = await contract.balanceOf(user2);
            assert.equal(balance, 10, 'The user did not receive the tokens');
        });
        it('Should send tokens from a user to a user', async () => {
            await contract.transferFrom(user2, user1, 10);
            const balance = await contract.balanceOf(user1);
            assert.equal(balance, 10, 'The user did not receive the tokens');
        });
        it('Should fail when transfering more money than the user has', async () => {
            const initialBalance = await contract.balanceOf(sender);
            await contract.transfer(user2, defaultSupply + 1);
            const balanceAfterOperation = await contract.balanceOf(sender);
            assert.equal(initialBalance.toNumber(), balanceAfterOperation.toNumber(), 'The transfer did not fail');
        });
    });
    
    describe('Sender Operations', () => {
        it('Should burn some coins', async () => {
            await contract.burn(sender, 100);
            const supply = await contract.totalSupply();
            assert.equal(supply, defaultSupply - 100, 'No coins were burned');
        });
        it('Should add some coins', async () => {
            await contract.mint(sender, 200);
            const supply = await contract.totalSupply();
            assert.equal(supply, defaultSupply + 200, 'No coins were added');
        });
        it('Should fail to add some coins because the user is not the sender', async () => {
            if(!revert(contract.mint, [user2, 100, {from: user1}])) throw new Error('User was able to add some coins');
        });
        it('Should fail to burn some coins because the user is not the sender', async () => {
            if(!revert(contract.burn, [user2, 100, {from: user1}])) throw new Error('User was able to burn some coins');
        });
        it('Should transfer ownership and check it by invoking an \'onlyOwner\' func by the new owner', async () => {
            await contract.transferOwnership(user1);
            await contract.mint(user1, 100, {from: user1});
            const supply = await contract.totalSupply();
            assert.equal(supply, defaultSupply + 100, 'The new owner was not able to perform an owner operation');
        })

    })
})